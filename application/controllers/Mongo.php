<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mongo extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   $this->load->helper('language');
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{	

		require 'vendor/autoload.php';

		// Creo un alias del namespace
		use MongoDB\Client as Mongo;

		// Crea una instancia del driver MongoDB
		$mongo= new Mongo("mongodb://localhost:27017");

		// Selecciona la base de datos llamada "pruebas"
		$dbPruebas = $mongo->prueba;

		// Selecciona la colección llamada "usuarios" de la base de datos "pruebas"
		$usuarios = $dbPruebas->usuarios;

		// Inserta un nuevo usuario en la colección
		$usuarios->insertOne(["usuario" => "antonio", "pass" => "123456"]);

		// Coge todos los documentos de la colección
		$cursor = $usuarios->find()->toArray();

		// Recorre el array de documentos
		foreach ($cursor as $usuario){
		 print_r($usuario);
		}
	}

	
}
