$(document).ready(function(){

var current_fs, next_fs, previous_fs; //fieldsets
var opacity;
var current = 1;
var steps = $("fieldset").length;

setProgressBar(current);

$(".next").click(function(){

	if (v.form()) {

		current_fs = $(this).parent();
		next_fs = $(this).parent().next();

		//Add Class Active
		$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

		//show the next fieldset
		next_fs.show();
		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
		step: function(now) {
		// for making fielset appear animation
		opacity = 1 - now;

		current_fs.css({
		'display': 'none',
		'position': 'relative'
		});
		next_fs.css({'opacity': opacity});
		},
		duration: 500
		});
		setProgressBar(++current);
	};
});

$(".previous").click(function(){

current_fs = $(this).parent();
previous_fs = $(this).parent().prev();

//Remove class active
$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

//show the previous fieldset
previous_fs.show();

//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
previous_fs.css({'opacity': opacity});
},
duration: 500
});
setProgressBar(--current);
});

function setProgressBar(curStep){
var percent = parseFloat(100 / steps) * curStep;
percent = percent.toFixed();
$(".progress-bar")
.css("width",percent+"%")
}

$(".submit").click(function(){


	event.preventDefault(); //prevent default action 
	var post_url = $("#msform").attr("action"); //get form action url
	var request_method = $("#msform").attr("method"); //get form GET/POST method
	var form_data = $("#msform").serialize(); //Encode form elements for submission
	
	$.ajax({
		url : post_url,
		type: 'POST',
		data : form_data,
		beforeSend: function() {
	        // setting a timeout
	        $('.enviar').addClass('disable');
	        $('.gif').show('slow');
	        $('.message').hide("slow");
	        $('.enviando-formulario').addClass("active-envio");
	        $('.primer-mensaje').addClass("active-mensaje");
	    },
	    success: function(response){
          	current_fs = $(this).parent();
  			next_fs = $(this).parent().next();

  			//Add Class Active
  			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

  			//show the next fieldset
  			next_fs.show();
  			//hide the current fieldset with style
  			current_fs.animate({opacity: 0}, {
  			step: function(now) {
  			// for making fielset appear animation
  			opacity = 1 - now;

  			current_fs.css({
  			'display': 'none',
  			'position': 'relative'
  			});
  			next_fs.css({'opacity': opacity});
  			},
  			duration: 500
  			});
  			setProgressBar(++current);
  			$('.primer-mensaje').removeClass("active-mensaje");
  			$('.segundo-mensaje').addClass("active-mensaje");
  			window.location.replace(base_url+"home/");
	    }
	});


return false;
})

});

function changeValue(input, value){	
    $('#'+input).html('<input type="hidden" name="'+input+'" value="'+value+'">');
}