
<?php $leng = $this->config->item('language_abbr');
        if ($leng == 'ar'){
        	$this->lang->load('web_lang','spanish');
			$shortname = "es";
			$language = "spanish";
		}


		if ($leng == 'br'){
			$this->lang->load('web_lang','portuguese');
			$shortname = "pt";
			$language = "portuguese";
		}

?>
<nav role="navigation" style="position:relative;text-align:left; background-color:#003399; padding-top:5px; padding-bottom:5px;">
	<div class="row m-0">
	    <div class="col-7 col-md-6 logo d-inline-flex align-items-center justify-content-center" style="position:relative;">
			<span style="margin-right:auto;color:#fff;font-size: 14px;font-weight:bold; padding-left:0px;margin-left:0px;font-family:Arial"><?=$this->lang->line('hlbax')?></span>
	    </div>
       	<div class="col-5 col-md-6  d-flex align-items-center justify-content-center">
	        <a href="#" style="margin-left:auto;">
	            <img style="height:auto;padding-bottom: 5px;max-width: 80px;" src="https://addtest.live/descubrehillrom/asset/img/LogoBaxter-01.png" class="logo-wexll">
	        </a>
        </div> 
	</div>
</nav>
<div class="custom-radio-button">
    <a href="<?php echo base_url().'ar'.uri_string()?>/<?php if(isset($_GET['id'])):?>?id=<?=$_GET['id']?> <?php endif; ?>" <?php if($language == 'spanish'):?> class="active" <?php endif; ?>>Español</a>
	<a href="<?php echo base_url().'br'.uri_string()?>/<?php if(isset($_GET['id'])):?>?id=<?=$_GET['id']?> <?php endif; ?>" <?php if($language == 'portuguese'):?> class="active" <?php endif; ?>>Portugués</a>
</div>
<header>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-6 col-md-3">
				<a href="<?=base_url()?>home/">
					<img src="<?=base_url()?>asset/img/logo_hl_bl.png" class="img-fluid logo-img">
				</a>
			</div>
			<div class="col-12 d-none d-sm-flex align-items-center flex-column justify-content-center col-md-6 text-center">
				
				<i class="fas fa-edit icon-edit" style="display:none;"></i>
				<label style="font-size: 12px;color: #001871;font-family: 'Matter-Regular';margin: 0;"><b>Demo:</b>
				<div class="select-dropdown">
					<button href="#" role="button" data-value="" class="select-dropdown__button"><span><?=$this->session->userdata('demo_title')?> - <?=$this->session->userdata('product')?></span> <i class="zmdi zmdi-chevron-down"></i>
					</button>
					<ul class="select-dropdown__list">
						 <?php foreach($demos['demo'] as $demo):
						 	$product = $this->page_model->get_product($demo['products'][0]);
							?>
							<li data-value="<?=base_url()?>home/demochange?id=<?=$demo['id']?>" class="select-dropdown__list-item"><?=$demo['title']?> - <?=$product['product'][0]['name']?></li>
						<?php endforeach; ?>
					</ul>
				</div>
				</label>
				<p><b>Hospital:</b> <?=$this->session->userdata('hospital');?> |
					<b>Inicio:</b> <?=date('Y-d-m',strtotime($this->session->userdata('demo_initial_date')))?> / <b>Fin:</b> <?=date('Y-d-m',strtotime($this->session->userdata('demo_end_date')))?></p>
			</div>
			<div class="col-6 col-md-3 d-flex align-items-center justify-content-end">
				<a href="<?=base_url()?>home/" class="homeicon">
					<i class="fas fa-home"></i>
				</a>
				<a href="#" class="notif notif-open">
					<?php if(!empty($notifications['count'])): ?>
						<div class="number-notif"><?=$notifications['count']?></div>
					<?php endif; ?>
					<img src="<?=base_url()?>asset/img/icon-campana.png" class="img-fluid">
				</a>
				<div class="user-name menu-open">
					<h6><?=substr($this->session->userdata('user_name'), 0, 1)?><?=substr($this->session->userdata('user_lastname'), 0, 1)?></h6>
				</div>
			</div>
		</div>
	</div>
</header>
<div class="col-12 d-flex d-sm-none text-center">
		<i class="fas fa-edit icon-edit" style="cursor: pointer;margin-top: 5px;font-size: 15px;color: #5369e5;"></i>
		<p><?=$this->session->userdata('hospital');?> - <?=$this->session->userdata('demo_title')?> - <?=$this->session->userdata('product')?> | Inicio: <?=date('Y-d-m',strtotime($this->session->userdata('demo_initial_date')))?> / Fin: <?=date('Y-d-m',strtotime($this->session->userdata('demo_end_date')))?></p>
</div>

<div class="nav-menu-animated">
	<div class="close-menu">X</div>
	<div class="row w-100 flex-column">
		<ul>
			<li><h4 style="color:#fff;margin-bottom:15px;"><?=$this->session->userdata('user_name')?> <?=$this->session->userdata('user_lastname')?><br><small>Rol: <?=$this->session->userdata('user_rol')?></small></h4></li>
			<li><a href="<?=base_url()?>encuesta/"><?=$this->lang->line('lbl_menu_02')?></a></li>
			<li><a href="<?=base_url()?>procedimientos/"><?=$this->lang->line('lbl_menu_01')?></a></li>
			<li><a href="<?=base_url()?>entrenamientos/"><?=$this->lang->line('lbl_menu_03')?></a></li>
			<li><a href="<?=base_url()?>estadisticas/"><?=$this->lang->line('lbl_menu_04')?></a></li>
			<li><a href="<?=base_url()?>perfil/"><?=$this->lang->line('lbl_perfil')?></a></li>
			<li><a href="<?=base_url()?>login/logout/"><?=$this->lang->line('lbl_salir')?></a></li>
		</ul>
	</div>
</div>

<div class="nav-menu-animated-notif">
	<div class="close-menu-notif">X</div>
	<div class="row w-100 flex-column">
		<h4 class="w-100"><?=$this->lang->line('lbl_title_enc')?></h4>
		<?php if(!empty($procedures)): $i=0; ?>
			<?php foreach($procedures as $key => $proc):
					if($proc['procedure'][0]['demo']['id'] == $this->session->userdata('id_demo')):
					$date_new = explode("T",$proc['procedure'][0]['endDate']);
					$date_new_final = $date_new[0];
						if(time() > strtotime($date_new_final)):?>
							<p>
								<a href="<?=base_url()?>encuesta/ver?id=<?=$proc['procedure'][0]['id']?>">
								<?php if(!empty($proc['procedure'][0]['category']['image']['url'])): ?>
		                            <img src="<?=$proc['procedure'][0]['category']['image']['url']?>" style="max-width: 30px;">
		                        <?php endif; ?> <?=$proc['procedure'][0]['title']?> - <?=date('Y-d-m',strtotime($proc['procedure'][0]['endDate']))?>
		                        </a>
		                    </p>
						<?php else: ?>
							<p style="opacity:.5;pointer-events:none;margin: 0 0 5px;">
								<?=$this->lang->line('lbl_prox')?> <?php if(!empty($proc['procedure'][0]['category']['image']['url'])): ?>
		                            <img src="<?=$proc['procedure'][0]['category']['image']['url']?>" style="max-width: 30px;">
		                        <?php endif; ?> <?=$proc['procedure'][0]['title']?> - <?=date('Y-d-m',strtotime($proc['procedure'][0]['endDate']))?></p>
						<?php endif; ?>
					<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>