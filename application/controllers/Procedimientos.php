<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Procedimientos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   $this->load->helper('language');
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{	
		if(!empty($this->session->userdata('token'))):
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    $this->template->add_css('asset/fullcalendar/fullcalendar.min.css');
		//añadimos los archivos js que necesitemoa		
		$this->template->add_js('asset/fullcalendar/lib/moment.min.js');
		$this->template->add_js('asset/fullcalendar/fullcalendar.min.js');
		$this->template->add_js('asset/js/event.js?v='.time().'');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		$this->template->add_js('asset/js/encuesta.js?v='.time().'');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Hillrom - GSS', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	
		$leng = $this->config->item('language_abbr');
			//Choose language file according to selected lanaguage
			//print_r($language);
			//exit;
			if($language == "portuguese"):
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = $language;
			elseif ($language == "spanish"):
				$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = $language;
			else:
				
	            if ($leng == 'ar'){
	            	$this->lang->load('web_lang','spanish');
					$data['shortname'] = "es";
					$data['language'] = "spanish";
				}


				if ($leng == 'br'){
					$this->lang->load('web_lang','portuguese');
					$data['shortname'] = "pt";
					$data['language'] = "portuguese";
				}

			endif;

		$data['categorias'] = $this->page_model->get_categorias();
		$demo = $this->page_model->get_demo_id($this->session->userdata('id_demo'));
		$product = $this->page_model->get_product($demo['demo'][0]['products'][0]);
		$data['survey'] = $this->page_model->get_survey($product['product'][0]['survey']);

		$data['notifications'] = $this->page_model->get_notificacions();
		$data['procedures'] = $this->page_model->get_procedures_users();
		$data['demos'] = $this->page_model->get_demo();
		
		$this->template->write_view('content', 'layout/procedimientos/main', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
	    
		//$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	    else:
			redirect('/');
		endif;
	}

	public function all()
	{	

			$leng = $this->config->item('language_abbr');
			//Choose language file according to selected lanaguage
			//print_r($language);
			//exit;
			if($language == "portuguese"):
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = $language;
			elseif ($language == "spanish"):
				$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = $language;
			else:
				
	            if ($leng == 'ar'){
	            	$this->lang->load('web_lang','spanish');
					$data['shortname'] = "es";
					$data['language'] = "spanish";
				}


				if ($leng == 'br'){
					$this->lang->load('web_lang','portuguese');
					$data['shortname'] = "pt";
					$data['language'] = "portuguese";
				}

			endif;
		$notify_user = $this->page_model->get_notificacions();
		foreach($notify_user['notifys']->notifications as $v):
			$procedure = $this->page_model->get_procedure($v->_id);
			foreach($procedure['procedure'] as $key => $proc):
				if($proc['demo']['id'] == $this->session->userdata('id_demo')):
					if($leng == 'ar'):
						$catLang = $proc['category']['spName'];
					else:
						$catLang = $proc['category']['ptName'];
					endif;

					$date_new = explode("T",$proc['endDate']);
					$date_new_final = $date_new[0];

					if(time() > strtotime($date_new_final)):
						$ev[] = array(

							'id' => $key,
							'title' => $proc['title'],
							'className' => array('completado'),
							'description' => $proc['description'],
							'encuesta_title' => $proc['title'],
							'link' => '/encuesta/ver/?id='.$proc['id'].'',
							'cat_titulo' => $catLang,
							'image_cat' => $proc['category']['image']['url'],
							'base_url' => base_url(),
							'start' => $proc['startDate'],
							'end' => $proc['endDate'],
							'status' => 1

						);
					else:
						$ev[] = array(

							'id' => $key,
							'title' => $proc['title'],
							'className' => array('proximo'),
							'description' => $proc['description'],
							'encuesta_title' => $proc['title'],
							'link' => '/encuesta/ver/?id='.$proc['id'].'',
							'cat_titulo' => $catLang,
							'image_cat' => $proc['category']['image']['url'],
							'base_url' => base_url(),
							'start' => $proc['startDate'],
							'end' => $proc['endDate'],
							'status' => 1

						);
					endif;
				endif;
			endforeach;
		endforeach;

		echo json_encode($ev);
		
	}

	public function add()
	{

		$data_send = array(
			"title" => $_POST['title'],
			"description" => $_POST['descripcion'],
			"startDate" => $_POST['startDate'],
			"category" => $_POST['category'],
			"endDate" => $_POST['endDate'],
			"survey" => $_POST['survey'],
			"demo" => $_POST['demo'],

		);
		
		$url = HOST.'procedure/';
		$curl = curl_init($url);

       	$authorization = "Authorization: Bearer ".$this->session->userdata('token');
	   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
       	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data_send));
       	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       	$response = curl_exec($curl);

       	print_r($response);
       	curl_close($curl);
	}

	
}
