console.log(quantity);
const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: lebels,
        datasets: [{
            label: '# of Votes',
            data: quantity,
            backgroundColor: [
                'rgb(176, 118, 226)',
                'rgb(83, 105, 229)',
                'rgb(117, 145, 174)',
                'rgb(75, 192, 192)',
                'rgb(0, 24, 113)',
                'rgb(129, 141, 210)',
                'rgb(183, 160, 209)',
                'rgb(27, 45, 144)'
            ],
            borderColor: [
                'rgba(176, 118, 226, 1)',
                'rgba(83, 105, 229, 1)',
                'rgba(117, 145, 174, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(0, 24, 113, 1)',
                'rgb(129, 141, 210)',
                'rgb(183, 160, 209)',
                'rgb(27, 45, 144)'
            ],
            borderWidth: 1,
            pointRadius: 5,
        }]
    },
    options: {
        plugins: {
            legend: {
                position: 'bottom',
                usePointStyle: true,
            }
        }
    }
});