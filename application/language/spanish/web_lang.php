<?php 
$lang['language_key'] = 'The actual message to be shown';

$lang['no_encuestas'] = 'No existen encuestas pendientes';
$lang['no_estadisticas'] = 'No existen estadísticas para visualizar';

$lang['input_user'] = 'USUARIO';
$lang['input_pass'] = 'CONTRASEÑA';
$lang['input_pass_rem'] = '¿OLVDIASTE TU CONSTRASEÑA?';
$lang['input_err'] = 'Usuario o contraseña incorrecto';

$lang['lbl_menu_01'] = 'PROCEDIMIENTOS';
$lang['lbl_menu_02'] = 'ENCUESTAS';
$lang['lbl_menu_03'] = 'DOCUMENTACIÓN';
$lang['lbl_menu_04'] = 'ESTADÍSTICAS';

$lang['lbl_title_enc'] = 'Encuestas:';
$lang['lbl_prox'] = 'Próxima:';
$lang['lbl_link'] = 'Ir a encuesta';

$lang['lbl_perfil'] = 'Mi perfil';
$lang['lbl_salir'] = 'Salir';

$lang['enc_titulo'] = 'ENCUESTA';
$lang['enc_btn_sig'] = 'Siguiente';
$lang['enc_btn_atr'] = 'Atrás';
$lang['enc_btn_env'] = 'Enviar';
$lang['enc_msj01'] = 'Por favor seleccione una respuesta';
$lang['enc_pregdeter'] = '¿Qué probabilidades hay de que recomiendes la compra de esta mesa quirúrgica?';
$lang['enc_com'] = 'Comentarios';

$lang['lbl_meses'] = '["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]';
$lang['lbl_dias'] = '["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"]';
$lang['hoy'] = 'Hoy';

$lang['lbl_galeria'] = 'Galeria de imagenes';
$lang['lbl_documentacion'] = 'Documentación educativa';
$lang['lbl_descargar'] = 'Descargar';

$lang['hlbax'] = 'Hillrom es parte de Baxter';
$lang['no_demo'] = 'El usuario no contiene una demo asociada.';

?>