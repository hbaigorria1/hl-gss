<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 text-center">

            <h4><?=$procedure['procedure'][0]['title']?> (<?=$procedure['procedure'][0]['startDate']?> - <?=$procedure['procedure'][0]['endDate']?>)</h4>
        </div>
        <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-5 text-center p-0">
                <div class="card p-0 m-0">
                    <form id="msform" method="post" action="<?=base_url().$this->config->item('language_abbr')?>/encuesta/send/">
                        <input type="hidden" name="id_user" value="<?=$this->session->userdata('id_user')?>">
                        <input type="hidden" name="id_procedure" value="<?=$procedure['procedure'][0]['id']?>">
                        <input type="hidden" name="id_demo" value="<?=$procedure['procedure'][0]['demo']['id']?>">
                        <!-- progressbar -->
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                        </div> <br> <!-- fieldsets -->
                        <?php 
                        if($shortname == 'pt'):
                            $lengQuestion = $procedure['procedure'][0]['survey']['ptQuestions'];
                        else:
                            $lengQuestion = $procedure['procedure'][0]['survey']['esQuestions'];
                        endif; ?>
                        <?php $last_key = end(array_keys($procedure['procedure'][0]['survey']['esQuestions'])); ?>
                        <?php foreach($lengQuestion as $kq => $qe): ?>
                                <?php $input = $this->page_model->slugify($qe['title']); ?>
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row title-degrade-form">
                                            <div class="col-7">
                                                <h2 class="fs-title"><?=$this->lang->line('enc_titulo')?>:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps"><?=$kq+1?>/<?=$last_key+1?></h2>
                                            </div>
                                        </div>
                                        <div class="content-inputs">
                                            <h5><?=$qe['title']?></h5>
                                            <h6><?=$qe['question']?></h6>
                                            <input type="hidden" name="question_title_<?=$kq?>" value="<?=$qe['title']?>">
                                            <p><?=$this->lang->line('enc_msj01')?></p>
                                            <div class="inputs-customs">
                                                <?php foreach($qe['values'] as $kv => $val):?>
                                                    <label class="radio-custom">
                                                      <input type="radio" name="valuequesiton_<?=$kq?>" onclick="changeValue('value_<?=$kq?>', '<?=$val['value']?>')"value="<?=$val['percentage']?>" >
                                                      <div id="value_<?=$kq?>"></div>
                                                      <span class="checkmark"><?=$val['value']?></span>
                                                    </label>
                                                <?php endforeach ?>
                                            </div>
                                            <?php if($kq == $last_key): ?>
                                                <div class="w-100">
                                                    <p><?=$this->lang->line('enc_com')?>:</p>
                                                    <textarea name="comentarios_<?=$kq?>"></textarea>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <?php if($kq != $last_key): ?>
                                        <input type="button" name="next" class="next action-button" value="<?=$this->lang->line('enc_btn_sig')?>" />
                                        <?php if($kq > 0): ?>
                                            <input type="button" name="previous" class="previous action-button-previous" value="<?=$this->lang->line('enc_btn_atr')?>" />
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <input type="button" name="next" class="submit action-button" value="<?=$this->lang->line('enc_btn_env')?>" /> <input type="button" name="previous" class="previous action-button-previous" value="<?=$this->lang->line('enc_btn_atr')?>" />
                                    <?php endif; ?>
                                </fieldset>
                        <?php endforeach; ?>
                        <div class="enviando-formulario">
                            <div class="primer-mensaje d-flex align-items-center justify-content-center">
                                <img src="<?=base_url()?>asset/img/gif-load.gif" class="img-fluid">
                                <p>Enviando encuesta</p>
                            </div>
                            <div class="segundo-mensaje d-flex align-items-center justify-content-center">
                                <p>Encuesta enviada con éxito</p>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js'></script>
<script>
        var v = $("#msform").validate({
              rules: {
                email: {
                  required: true
                },
                    <?php 
                    if($shortname == 'pt'):
                        $lengQuestion = $procedure['procedure'][0]['survey']['ptQuestions'];
                    else:
                        $lengQuestion = $procedure['procedure'][0]['survey']['esQuestions'];
                    endif; ?>
                    <?php foreach($lengQuestion as $kq => $qe): ?>
                        valuequesiton_<?=$kq?>: "required",
                    <?php endforeach; ?>
              },
              messages: {
                    <?php 
                    if($shortname == 'pt'):
                        $lengQuestion = $procedure['procedure'][0]['survey']['ptQuestions'];
                    else:
                        $lengQuestion = $procedure['procedure'][0]['survey']['esQuestions'];
                    endif; ?>
                    <?php foreach($lengQuestion as $kq => $qe): ?>
                        valuequesiton_<?=$kq?>: "Seleccione al menos una opción",
                    <?php endforeach; ?>
              },
              errorElement: "span",
              errorClass: "error",
              errorPlacement: function(error, element) {

                if (element.is(":radio")) 
                {
                    error.appendTo(element.parents('.inputs-customs') );
                }
                else 
                { // This is the default behavior 
                    error.insertBefore(element);
                }

                    
              }
        });
</script>