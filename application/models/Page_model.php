<?php

class page_model extends CI_Model
{


		public function __construct()
        {
			parent::__construct();
			// Your own constructor code
			$this->load->helper('cookie');
        }
        
        public $title;
        public $content;
        public $date;

        public function get_events($abr)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('events.*');
			$this->db->group_by('events.uniq');
			$this->db->where('lenguajes.abr', $abr);
			$this->db->join('lenguajes', 'lenguajes.id = events.lang');
			$result = $this->db->get('events');
			return $result->result();
		}
		

		public function get_survey($id)
		{
			$host = ''.HOST.'survey/'.$id.'';
		   	$curl = curl_init($host);

		   	$authorization = "Authorization: Bearer ".$this->session->userdata('token');
		   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

		    curl_setopt($curl, CURLOPT_HTTPGET, true);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        $response = curl_exec($curl);
	        curl_close($curl);
		    return json_decode($response, TRUE);

		}

		public function get_surveys()
		{
			$host = ''.HOST.'survey/all';
		   	$curl = curl_init($host);

		   	$authorization = "Authorization: Bearer ".$this->session->userdata('token');
		   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

		    curl_setopt($curl, CURLOPT_HTTPGET, true);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        $response = curl_exec($curl);
	        curl_close($curl);
		    return json_decode($response, TRUE);

		}

		public function get_statistics($id_demo)
		{
			$host = ''.HOST.'answeredSurvey/statistics/'.$id_demo.'';
		   	$curl = curl_init($host);

		   	$authorization = "Authorization: Bearer ".$this->session->userdata('token');
		   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

		    curl_setopt($curl, CURLOPT_HTTPGET, true);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        $response = curl_exec($curl);
	        curl_close($curl);
		    return json_decode($response, TRUE);

		}

		public function get_procedures()
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'procedure/all';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
			    return json_decode($response, TRUE);
		    else:
				return NULL;
			endif;

		}

		public function get_categorias()
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'category/all';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
			    return json_decode($response, TRUE);
		    else:
				return NULL;
			endif;

		}

		public function get_notificacions()
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'user/'.$this->session->userdata('id_user').'';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $notify = curl_exec($curl);
		        curl_close($curl);
		        $notifys = json_decode($notify);
		        
		        foreach($notifys->notifications as $not):
		        	if($not->demo == $this->session->userdata('id_demo')):
						$date_new = explode("T",$not->endDate);
						$date_new_final = $date_new[0];
						if(time() > strtotime($date_new_final)):
							$count[] = array(
								'array' => $not->endDate
							);
			        	endif;
		        	endif;
		        endforeach;
			    return array('notifys' => $notifys, 'count' => count($count));
		    else:
				return NULL;
			endif;

		}

		public function get_user()
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'user/'.$this->session->userdata('id_user').'?offPagination=true';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
			    return json_decode($response, TRUE);
		    else:
				return NULL;
			endif;

		}


		public function get_demo()
		{
			//if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'demo/user/'.$this->session->userdata('id_user').'';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
		        if($response):
			    	return json_decode($response, TRUE);
				else:
					return "no";
				endif;
		    //else:
			//	return NULL;
			//endif;

		}

		public function get_demo_user($id, $token)
		{
			if(!empty($token)):
				$host = ''.HOST.'demo/user/'.$id.'';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$token;
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
			    return json_decode($response, TRUE);
		    else:
				return NULL;
			endif;

		}

		public function get_demo_id($id_demo)
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'demo/'.$id_demo.'';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
			    return json_decode($response, TRUE);
		    else:
				return NULL;
			endif;

		}

		public function get_asset($id_product)
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'asset/product/'.$id_product.'';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
			    return json_decode($response, TRUE);
		    else:
				return NULL;
			endif;

		}

		public function get_hospital($id_hosptial)
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'hospital/'.$id_hosptial.'';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
			    return json_decode($response, TRUE);
		    else:
				return NULL;
			endif;

		}

		public function get_product($id_product)
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'product/'.$id_product.'';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
			    return json_decode($response, TRUE);
		    else:
				return NULL;
			endif;

		}


		public function get_procedure($id)
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'procedure/'.$id.'';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $response = curl_exec($curl);
		        curl_close($curl);
			    return json_decode($response, TRUE);
			else:
				return NULL;
			endif;

		}

		public function get_procedures_users()
		{
			if(!empty($this->session->userdata('token'))):
				$host = ''.HOST.'user/'.$this->session->userdata('id_user').'';
			   	$curl = curl_init($host);

				$authorization = "Authorization: Bearer ".$this->session->userdata('token');
			   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

			    curl_setopt($curl, CURLOPT_HTTPGET, true);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		        $notify = curl_exec($curl);
		        curl_close($curl);
		        $notifys = json_decode($notify);
		       
		        foreach($notifys->notifications as $not):
		        	$procedure = $this->get_procedure($not->_id);
		        	$procedures[] = $procedure;
	
		        endforeach;
        	 	return $procedures;
		    else:
				return NULL;
			endif;
		}

		public function get_procedure_user()
		{
			$host = HOST.'answeredsurvey/user/'.$this->session->userdata('id_user').'';
		   	$curl = curl_init($host);

			$authorization = "Authorization: Bearer ".$this->session->userdata('token');
		   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

		    curl_setopt($curl, CURLOPT_HTTPGET, true);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        $response = curl_exec($curl);
	        curl_close($curl);
	        return json_decode($response, TRUE);
	        exit;
		    $userAnswere = json_decode($response, TRUE);
		    $data = '';
		    foreach($userAnswere as $key => $usAs):
		    	foreach($usAs as $ua):
		    		foreach($this->get_procedures() as $key => $pre):
		    			$data = $pre;
		    		endforeach;
		    	endforeach;
		    endforeach;
		    return $data;

		}


		public function get_user_procedure($id_procedure)
		{
			$host = ''.HOST.'answeredsurvey/procedure/'.$id_procedure.'/user/'.$this->session->userdata('id_user').'';
		   	$curl = curl_init($host);

			$authorization = "Authorization: Bearer ".$this->session->userdata('token');
		   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));

		    curl_setopt($curl, CURLOPT_HTTPGET, true);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	        $response = curl_exec($curl);
	        curl_close($curl);
		    return json_decode($response, TRUE);

		}

		public function slugify($text)
		{
		    // Strip html tags
		    $text=strip_tags($text);
		    // Replace non letter or digits by -
		    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
		    // Transliterate
		    setlocale(LC_ALL, 'en_US.utf8');
		    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		    // Remove unwanted characters
		    $text = preg_replace('~[^-\w]+~', '', $text);
		    // Trim
		    $text = trim($text, '-');
		    // Remove duplicate -
		    $text = preg_replace('~-+~', '-', $text);
		    // Lowercase
		    $text = strtolower($text);
		    // Check if it is empty
		    if (empty($text)) { return 'n-a'; }
		    // Return result
		    return $text;
		}
}

?>