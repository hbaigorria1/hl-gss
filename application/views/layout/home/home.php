<section class="home-sect-top">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 p-1">
				<div style="position:relative;">
					<img src="<?=base_url()?>asset/img/img-home-in.png" class="img-fluid w-100">
					<div class="absolite-img-tour">
						<img src="<?=base_url()?>asset/img/logo_tour_hl.png" class="img-fluid">
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 p-0 home-sect-btns d-none d-sm-flex">
				<div class="row w-100 m-0">
					<div class="col-6">
						<a href="<?=base_url()?>procedimientos/" class="contante-btn">
							<img src="<?=base_url()?>asset/img/icon-h-13.png" class="img-fluid" style="max-width: 35px;">
							<h4><?=$this->lang->line('lbl_menu_01')?></h4>
						</a>
					</div>
					<div class="col-6">
						<a href="<?=base_url()?>encuesta/" class="contante-btn" style="position:relative;">
							<?php if(!empty($notifications['count'])): ?>
								<div class="number-notif"><?=$notifications['count']?></div>
							<?php endif; ?>
							<img src="<?=base_url()?>asset/img/icon-h-14.png" class="img-fluid" style="max-width: 35px;">
							<h4 style="color:#5369e5"><?=$this->lang->line('lbl_menu_02')?></h4>
						</a>
					</div>
					<div class="col-6">
						<a href="<?=base_url()?>entrenamientos/" class="contante-btn">
							<img src="<?=base_url()?>asset/img/icon-h-15.png" class="img-fluid" style="max-width: 60px;">
							<h4 style="color:#001871;"><?=$this->lang->line('lbl_menu_03')?></h4>
						</a>
					</div>
					<div class="col-6">
						<a href="<?=base_url()?>estadisticas/" class="contante-btn">
							<img src="<?=base_url()?>asset/img/icon-h-16.png" class="img-fluid" style="max-width: 45px;">
							<h4 style="color:#7591ae;"><?=$this->lang->line('lbl_menu_04')?></h4>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="home-sect-btns d-block d-sm-none">
	<div class="container p-0">
		<div class="row w-100 m-0">
			<div class="col-6 p-1">
				<a href="<?=base_url()?>procedimientos/" class="contante-btn">
					<img src="<?=base_url()?>asset/img/icon-h-13.png" class="img-fluid">
					<h4><?=$this->lang->line('lbl_menu_01')?></h4>
				</a>
			</div>
			<div class="col-6 p-1">
				<a href="<?=base_url()?>encuesta/" class="contante-btn" style="position:relative;">
					<?php if(!empty($numberNotify)): ?>
						<div class="number-notif"><?=$numberNotify?></div>
					<?php endif; ?>
					<img src="<?=base_url()?>asset/img/icon-h-14.png" class="img-fluid">
					<h4 style="color:#5369e5"><?=$this->lang->line('lbl_menu_02')?></h4>
				</a>
			</div>
			<div class="col-6 p-1">
				<a href="<?=base_url()?>entrenamientos/" class="contante-btn">
					<img src="<?=base_url()?>asset/img/icon-h-15.png" class="img-fluid">
					<h4 style="color:#001871;"><?=$this->lang->line('lbl_menu_03')?></h4>
				</a>
			</div>
			<div class="col-6 p-1">
				<a href="<?=base_url()?>estadisticas/" class="contante-btn">
					<img src="<?=base_url()?>asset/img/icon-h-16.png" class="img-fluid">
					<h4 style="color:#7591ae;"><?=$this->lang->line('lbl_menu_04')?></h4>
				</a>
			</div>
		</div>
	</div>
</section>