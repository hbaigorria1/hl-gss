<section class="estadisticas">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-2"></div>
            <div class="col-12 col-md-8" style="background: rgb(228 233 239);">
                <div class="head-estaditicas d-flex align-items-center">
                <img src="<?=base_url()?>asset/img/icon-est.png" class="img-fluid">    
                <h3><?=$this->lang->line('lbl_menu_04')?></h3>
                </div>
                <?php if(!empty($statistics['statistics']['data'])): ?>
                <div class="content-bars">
                    <?php foreach($statistics['statistics']['data'] as $key => $sta): ?>
                        <div class="bars-desc">
                            <p><?=$sta['question']?> <span><?=$sta['percentage']?>%</span></p>
                            <div class="progress">
                              <div class="progress-bar" role="progressbar" style="width: <?=$sta['percentage']?>%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="stadiscts">
                    <canvas id="myChart" width="100%" height="400"></canvas>
                </div>
                <?php else: ?>
                    <div class="content-bars">
                        <p><?=$this->lang->line('no_estadisticas')?></p>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-12 col-md-2"></div>
        </div>
    </div>
</section>
<?php foreach($statistics['statistics']['answersOrdererByValue'] as $val):
    $value[] = $val['value'];
endforeach; ?>
<?php foreach($statistics['statistics']['answersOrdererByValue'] as $val2):
    $quantity[] = $val2['quantity'];
endforeach; ?>
<script type="text/javascript">
    var lebels = <?=json_encode($value)?>;
    var quantity = <?=json_encode($quantity)?>;
</script>