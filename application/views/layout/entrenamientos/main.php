<section class="entrenamientos">
  <?php foreach($demo['demo'][0]['products'] as $key => $product):
    $asset = $this->page_model->get_asset($product);
    $prod = $this->page_model->get_product($product);
      $pdf = '';
      $jpg = '';
      $mp4 = '';
    ?>
    <?php foreach($asset['assets'] as $key => $ass): if($ass['extension'] == 'pdf'): $pdf = 'si'; endif; endforeach; ?>
    <?php foreach($asset['assets'] as $key => $ass): if($ass['extension'] == 'mp4'): $mp4 = 'si'; endif; endforeach; ?>
    <?php foreach($asset['assets'] as $key => $ass): if($ass['extension'] == 'jpg' OR $ass['extension'] == 'png'): $jpg = 'si'; endif; endforeach; ?>
      <div class="container">
          <div class="row">
            <div class="col-12 col-md-2"></div>
              <div class="col-12 col-md-8 text-left">
                  <h4><?=$prod['product'][0]['name']?></h4>
                  <?php if(!empty($mp4)): ?>
                    <?php foreach($asset['assets'] as $key => $ass): ?>
                      <?php if($ass['extension'] == 'mp4'): ?>
                        <video tabindex="0" width="100%" height="auto" muted="" controls="controls" playsinline="" taborder="2">
                            <source src="<?=$ass['link']?>" type="video/mp4">
                        </video>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  <?php endif; ?>
              </div>
            <div class="col-12 col-md-2"></div>
          </div>
          
          <div class="row">
              <div class="col-12 col-md-2"></div>
              <div class="col-12 col-md-8">
                <div id="accordion">
                    <?php if(!empty($jpg) || !empty($png)): ?>
                    <div class="card">
                      <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <?=$this->lang->line('lbl_galeria')?>
                          </button>
                        </h5>
                      </div>

                      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                          <figure class="wp-block-gallery alignfull columns-3 is-cropped">
                            <ul class="blocks-gallery-grid">
                              <?php foreach($asset['assets'] as $key => $ass): ?>
                              <?php if($ass['extension'] == 'jpg' OR $ass['extension'] == 'png'): ?>
                                <li class="blocks-gallery-item">
                                  <figure><a href="<?=$ass['file']['url']?>"><img src="<?=$ass['file']['url']?>"></a></figure>
                                </li>
                              <?php endif; ?>
                              <?php endforeach; ?>
                            </ul>
                          </figure>
                        </div>
                      </div>
                    </div>
                    <?php endif; ?>
                    <?php if(!empty($pdf)): ?>
                      <div class="card">
                        <div class="card-header" id="headingTwo">
                          <h5 class="mb-0">
                            <button class="btn btn-link collapse show" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              <?=$this->lang->line('lbl_documentacion')?>
                            </button>
                          </h5>
                        </div>
                        <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                          <div class="card-body">
                            <div class="row w-100">
                              <?php foreach($asset['assets'] as $key => $ass): ?>
                                <?php if($ass['extension'] == 'pdf'): ?>
                                  <a href="<?=$ass['file']['url']?>" target="_blank" class="col-6 col-md-4 text-center">
                                    <h5><?=$ass['name']?></h5>
                                    <i class="far fa-file-pdf"></i>
                                    <p><?=$this->lang->line('lbl_descargar')?></p>
                                  </a>
                                <?php endif; ?>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                </div>
              </div>
              <div class="col-12 col-md-2"></div>
          </div>
      </div>
  <?php endforeach; ?>
</section>