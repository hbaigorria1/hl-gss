
<?php $demo = $this->page_model->get_demo();?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 custom-calendar">
            <div class="btn-proced">
                Agregar procedimiento
            </div>
            <div id="event-action-response"></div>
            <div id="calendar"></div>
        </div>
    </div>
</div>
<div class="modal-procedimiento d-none align-items-center justify-content-center">
    <div class="content-form">
        <form class="add-proc row m-0 w-100" action="<?=base_url().$this->config->item('language_abbr')?>/procedimientos/add/" method="post" class="w-100 row m-0">
            <div class="col-12">
                <h3>Agregar nuevo procedimiento</h3>
            </div>
            <input type="hidden" name="demo" value="<?=$this->session->userdata('id_demo')?>">
            <input type="hidden" name="survey" value="<?=$survey['survey'][0]['id']?>">
            <div class="col-12 col-md-4">
                <input type="text" name="title" placeholder="Titulo" required>
            </div>
            <div class="col-12 col-md-4">
                <input type="text" name="descripcion" placeholder="Descripcion" required>
            </div>
            <div class="col-12 col-md-4">
                <select name="category" required>
                    <option value="">Seleccionar categoria</option>
                    <?php foreach($categorias['categories'] as $value):  ?>
                        <?php 
                        if($shortname == 'pt'):
                            $lengvalue = $value['ptName'];
                        else:
                            $lengvalue = $value['spName'];
                        endif; ?>
                        <option value="<?=$value['id']?>"><?=$lengvalue;?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-12 col-md-6">
                <input type="datetime-local" name="startDate" class="startDateNew" placeholder="Fecha de inicio" required>
            </div>
            <div class="col-12 col-md-6">
                <input type="datetime-local" name="endDate" class="endDateNew" placeholder="Fecha de fin" required>
            </div>
            <div class="col-12">
                <input type="submit" class="agregar-proc" value="Agregar">
            </div>
        </form>
        <div class="cierre-modal-proc">
            X
        </div>
    </div>
</div>
<div class="modal-events d-none align-items-center justify-content-center">
    <div class="content-modal">
        <div class="contents-principal">
            <div class="row">
                <div class="col-6">
                    <h3 class="title-event">Titulo procedimiento</h3>
                    <p class="description-event">Descripcion</p>
                </div>
                <div class="col-6 text-center">
                    <h4 class="title-cat">Cirugía General</h4>
                    <div class="image-icon-cat"></div>
                </div>
            </div>
        </div>
        <hr>
        <div class="faja-event">
            <h5>Encuestas:</h5>
            <div class="link-modal">
            </div>
        </div>
        <div class="cierre-modal">
            X
        </div>
    </div>
</div>