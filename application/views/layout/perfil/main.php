<section class="form-login" style="padding:10px;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-2"></div>
			<div class="col-12 col-md-8">
				<h5 style="margin-bottom:25px;">Mi perfil:</h5>
				<form class="w-100 login-form" method="post" action="#">
					<div class="row">
						<div class="col-6">
							<p>Hospital:</p>
							<input type="text" name="hospital" value="Hospital" disabled>
						</div>
						<div class="col-6">
							<p>Rol:</p>
							<input type="text" name="rol" value="<?=$perfil['role']['spName']?>" disabled>
						</div>
						<div class="col-6">
							<p>Nombre:</p>
							<input type="text" name="nombre" value="<?=$perfil['name']?>" disabled>
						</div>
						<div class="col-6">
							<p>Apellido:</p>
							<input type="text" name="apellido" value="<?=$perfil['surename']?>" disabled>
						</div>
						<div class="col-6">
							<p>Email:</p>
							<input type="text" name="email" value="<?=$perfil['email']?>" disabled>
						</div>
						<div class="col-6">
							<p>Teléfono:</p>
							<input type="text" name="telefono" value="<?=$perfil['phoneNumber']?>" disabled>
						</div>
						<div class="col-6">
							<p>Usuario:</p>
							<input type="text" name="usuario" value="<?=$perfil['username']?>" disabled>
						</div>
						<div class="col-6">
							<?php if(false): ?>
							<p>¿Cambiar passowrd?:</p>
							<div class="passoword-input">
								<input type="password" name="password" required id="id_password">
								<i class="far fa-eye" id="togglePassword" style="cursor: pointer;"></i>
							</div>
							<input type="submit" value="CAMBIAR PASSWORD">
							<?php endif; ?>
						</div>
					</div>
					<?php if(false): ?>
					<div class="passoword-input">
						<input type="password" name="password" required id="id_password" placeholder="PASSWORD">
						<i class="far fa-eye" id="togglePassword" style="cursor: pointer;"></i>
					</div>
						<input type="submit" value="LOGIN">
					<?php endif; ?>
				</form>
			</div>
			<div class="col-12 col-md-2"></div>
		</div>
	</div>
</section>