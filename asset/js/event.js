$(document).ready(function() {
    var calendar = $('#calendar').fullCalendar({
        monthNames: JSON.parse(meses),
        dayNamesShort: JSON.parse(dias),
        buttonText: {today: hoy},
        editable:true,
        
        events: base_url+'/procedimientos/all/',
        selectable:true,
        selectHelper:true,
        eventClick: function(info) {
            $('.modal-events').removeClass('d-none');
            $('.modal-events').addClass('d-flex');
            $('.title-event').html(info.title);
            $('.title-cat').html(info.cat_titulo);
            $('.image-icon-cat').html('<img src="'+info.image_cat+'" class="img-fluid">');
            $('.description-event').html(info.description);
            if(info.status == 1){
                $('.link-modal').html('<a href="'+info.base_url+''+info.link+'" class="item-encuesta realizada">'+info.encuesta_title+' <i class="fas fa-check"></i></a>');
            }
            if(info.status == 0){
                $('.link-modal').html('<a href="'+info.base_url+''+info.link+'" class="item-encuesta pendiente">'+info.encuesta_title+' <i class="far fa-clock"></i></a>');
            }
        },
    });
});

$( ".cierre-modal" ).click(function() {
  $('.modal-events').removeClass('d-flex');
  $('.modal-events').addClass('d-none');
});

$( ".cierre-modal-proc" ).click(function() {
  $('.modal-procedimiento').removeClass('d-flex');
  $('.modal-procedimiento').addClass('d-none');
});

$( ".btn-proced" ).click(function() {
  $('.modal-procedimiento').removeClass('d-none');
  $('.modal-procedimiento').addClass('d-flex');
});