<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Encuesta extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   $this->load->helper('language');
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{	
		if(!empty($this->session->userdata('token'))):
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/encuesta.css?v='.time().'');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		$this->template->add_js('asset/js/encuesta.js?v='.time().'');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Hillrom - GSS', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	
		$leng = $this->config->item('language_abbr');
			//Choose language file according to selected lanaguage
			//print_r($language);
			//exit;
			if($language == "portuguese"):
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = $language;
			elseif ($language == "spanish"):
				$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = $language;
			else:
				
	            if ($leng == 'ar'){
	            	$this->lang->load('web_lang','spanish');
					$data['shortname'] = "es";
					$data['language'] = "spanish";
				}


				if ($leng == 'br'){
					$this->lang->load('web_lang','portuguese');
					$data['shortname'] = "pt";
					$data['language'] = "portuguese";
				}

			endif;
		$data['notifications'] = $this->page_model->get_notificacions();
		$data['procedures'] = $this->page_model->get_procedures_users();
 		$data['demos'] = $this->page_model->get_demo();


		$this->template->write_view('content', 'layout/encuesta/main', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
	    
		//$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	    else:
			redirect('/');
		endif;
	}

	public function ver()
	{	
		if(!empty($this->session->userdata('token'))):
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/encuesta.css?v='.time().'');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		$this->template->add_js('asset/js/encuesta.js?v='.time().'');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Hillrom - GSS', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	
		$leng = $this->config->item('language_abbr');
			//Choose language file according to selected lanaguage
			//print_r($language);
			//exit;
			if($language == "portuguese"):
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = $language;
			elseif ($language == "spanish"):
				$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = $language;
			else:
				
	            if ($leng == 'ar'){
	            	$this->lang->load('web_lang','spanish');
					$data['shortname'] = "es";
					$data['language'] = "spanish";
				}


				if ($leng == 'br'){
					$this->lang->load('web_lang','portuguese');
					$data['shortname'] = "pt";
					$data['language'] = "portuguese";
				}

			endif;
		$data['procedure'] = $this->page_model->get_procedure($_GET['id']);
		//print_r($data['procedure']['procedure'][0]['survey']);
		$data['surveys'] = $this->page_model->get_survey($data['procedure']['procedure'][0]['survey']);
 		$data['notifications'] = $this->page_model->get_notificacions();
 		$data['procedures'] = $this->page_model->get_procedures_users();
 		$data['demos'] = $this->page_model->get_demo();
 		
		$this->template->write_view('content', 'layout/encuesta/ver', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
	    
		//$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');

	    else:
			redirect('/');
		endif;
	}

	public function send()
	{
		$procedure = $this->page_model->get_procedure($_POST['id_procedure']);
		$surveys = $this->page_model->get_survey($procedure['procedure'][0]['survey']['id']);
		
		foreach($procedure['procedure'][0]['survey']['esQuestions'] as $kq => $qe):
			$data_survey[] = array(
					"question" => $_POST['question_title_'.$kq.''],
					"value" => $_POST['value_'.$kq.''],
					"percentage" => $_POST['valuequesiton_'.$kq.''],
					"comment" => $_POST['comentarios_'.$kq.'']
					
			);
			
		endforeach;
			$data_send = array(
				"questions" => $data_survey,
				"user" => $_POST['id_user'],
				"procedure" => $_POST['id_procedure'],
				"demo" => $_POST['id_demo'],

			);
		$url = HOST.'answeredSurvey/';
		$curl = curl_init($url);

       	$authorization = "Authorization: Bearer ".$this->session->userdata('token');
	   	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
       	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data_send));
       	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       	$response = curl_exec($curl);
       	curl_close($curl);

	}
	
}