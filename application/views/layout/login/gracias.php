<section class="section-home-01">
	<div class="col-12 p-0">
		<img src="<?=base_url()?>asset/img/logo_hl.png" class="img-fluid logo-hl">
	</div>
	<div class="col-12 p-0 text-center">
		<img src="<?=base_url()?>asset/img/logo_tour_hl.png" class="img-fluid logo-tour">
	</div>
</section>

<section class="form-login">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-3"></div>
			<div class="col-12 col-md-6">
				<p>Su contraseña se recuperó con éxito, por favor revise su casilla de correo para obtner mas información.</p>
				<a href="<?=base_url()?>" style="text-decoration:underline;">Volver al inicio</a>
			</div>
			<div class="col-12 col-md-3"></div>
		</div>
	</div>
</section>