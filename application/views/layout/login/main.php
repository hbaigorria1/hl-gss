<?php $leng = $this->config->item('language_abbr');
        if ($leng == 'ar'){
        	$this->lang->load('web_lang','spanish');
			$shortname = "es";
			$language = "spanish";
		}


		if ($leng == 'br'){
			$this->lang->load('web_lang','portuguese');
			$shortname = "pt";
			$language = "portuguese";
		}

?>
<nav role="navigation" style="position:relative;text-align:left; background-color:#003399; padding-top:5px; padding-bottom:5px;">
	<div class="row m-0">
	    <div class="col-7 col-md-6 logo d-inline-flex align-items-center justify-content-center" style="position:relative;">
			<span style="margin-right:auto;color:#fff;font-size: 14px;font-weight:bold; padding-left:0px;margin-left:0px;font-family:Arial">Hillrom is a part of Baxter</span>
	    </div>
       	<div class="col-5 col-md-6  d-flex align-items-center justify-content-center">
	        <a href="#" style="margin-left:auto;">
	            <img style="height:auto;padding-bottom: 5px;max-width: 80px;" src="https://addtest.live/descubrehillrom/asset/img/LogoBaxter-01.png" class="logo-wexll">
	        </a>
        </div> 
	</div>
</nav>

<section class="section-home-01" style="position:relative;">
	<div class="custom-radio-button" style="position:absolute;right:0;top:0;">
	    <a href="<?php echo base_url().'ar'.uri_string()?>/" <?php if($language == 'spanish'):?> class="active" <?php endif; ?> style="color:#fff">Español</a>
		<a href="<?php echo base_url().'br'.uri_string()?>/" <?php if($language == 'portuguese'):?> class="active" <?php endif; ?> style="color:#fff">Portugués</a>
	</div>
	<div class="col-12 p-0">
		<img src="<?=base_url()?>asset/img/logo_hl.png" class="img-fluid logo-hl">
	</div>
	<div class="col-12 p-0 text-center">
		<img src="<?=base_url()?>asset/img/logo_tour_hl.png" class="img-fluid logo-tour">
	</div>

</section>

<section class="form-login">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-3"></div>
			<div class="col-12 col-md-6">
				<form class="w-100 login-form" method="post" action="<?=base_url().$this->config->item('language_abbr')?>/login/user-login/">
					<input type="text" name="user" required placeholder="<?=$this->lang->line('input_user')?>">
					<div class="passoword-input">
						<input type="password" name="password" required id="id_password" placeholder="<?=$this->lang->line('input_pass')?>">
						<i class="far fa-eye" id="togglePassword" style="cursor: pointer;"></i>
					</div>
					<?php if(false): ?>
					<div class="col-12 text-right p-0">
						<a href="<?=base_url()?>login/password/" class="link-pass"><?=$this->lang->line('input_pass_rem')?></a>
					</div>
					<?php endif; ?>
					<input type="submit" value="ENTRAR">
				</form>
			</div>
			<div class="col-12 col-md-3"></div>
			<div class="col-12 col-md-3"></div>
			<div class="col-12 col-md-6">
				<div class="show-login-message" style="display:none;font-family: 'Matter-Medium';color: red;font-weight: normal;font-size: 14px;"><?=$this->lang->line('input_err')?></div>
				<div class="show-login-message-no-demo" style="display:none;font-family: 'Matter-Medium';color: red;font-weight: normal;font-size: 14px;"><?=$this->lang->line('no_demo')?></div>
			</div>
			<div class="col-12 col-md-3"></div>
		</div>
	</div>
</section>