var markersArrayP=[];
var markerCluster;
var map;
var bounds;
$(document).ready(function(){
	
	  var myLatlng = new google.maps.LatLng(-35.4327369,-72.1046738);
     	  var myOptions = {
    	  zoom: 5,
    	  center: myLatlng,
    	  mapTypeId: google.maps.MapTypeId.ROADMAP,
    	  scrollwheel: false,
    	  streetViewControl: true
    	};
    	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        bounds = new google.maps.LatLngBounds();

		$(window).scroll(function () {
			if ($(this).scrollTop() > $('.consecionarios-content').offset().top) {				
				$('.buscador').addClass('fixed');
			}else {
				$('.buscador').removeClass('fixed');
			}		
		});
     
		var ii = 0;
		 $('.list-concesionario').each(function(){
			ii++; 
			if(ii==4){
				$(this).addClass('clear');
				ii=1;
			}
		 });

         
         $('#buscar_filtro').click(function(){
				 $('.list-concesionario').removeClass('clear');
                 var filtro = $('#filtros').val();
                 if(filtro=='0'){
                     $('.list-concesionario').show();
					 $('.list-concesionario-title').show();
					 var ii = 0;
					 $('.list-concesionario').each(function(){
						ii++; 
						if(ii==4){
							$(this).addClass('clear');
							ii=1;
						}
					 });
                 }else{
                     $('.list-concesionario').hide();
					  $('.list-concesionario-title').hide();
                     $('.list-concesionario[data-filter='+filtro+']').show();
					 $('.list-concesionario-title[data-filter='+filtro+']').show();
					 var ii = 0;
					 $('.list-concesionario[data-filter='+filtro+']').each(function(){
						ii++; 
						if(ii==4){
							$(this).addClass('clear');
							ii=1;
						}
					 });
                 }
                  /*$('html, body').animate({
                       scrollTop: $(".consecionarios-content").offset().top
                  }, 2000);*/
                  mostrarPlacemarks();
         });
         mostrarPlacemarks();
});

function mostrarPlacemarks(){
        for (var j = 0; j < markersArrayP.length; j++ ) {
          markersArrayP[j].setMap(null);
          markerCluster.clearMarkers();
          bounds = new google.maps.LatLngBounds();
         }   
        markersArrayP = [];
       
        
        var i=0;
        $('.list-concesionario').each(function(){
             i++;
             if($(this).is(":visible")){
                $(this).find('.marca').html(i); 
                var latcat=$(this).find('.latlong').html();
                var location=new google.maps.LatLng(latcat.split(',')[0],latcat.split(',')[1]);
	        var lab = $(this).find('.marca').html();
                var marker = new google.maps.Marker({
		   position: location,
		   map: map,
		   label: lab
	        });
                markersArrayP.push(marker);
                bounds.extend(marker.getPosition());
                google.maps.event.addListener(marker, 'click', function(e) {
		       $('html, body').animate({
                            scrollTop: $( ".marca:contains("+this.label+")" ).offset().top-120
                        }, 600);
                       $('.list-concesionario').removeClass('active');
                       $( ".marca:contains("+this.label+")" ).parent().addClass('active');
	        });
             }
         });
         map.fitBounds(bounds);
         markerCluster = new MarkerClusterer(map, markersArrayP);
         
     }

function resize(){

}

function obtenerProvincias(country_code){
	url=base_url+'home/getprovincias/?country_code='+country_code;
		$.ajax({
			url: url,
			success: function(response){	
				$('#provincia').html(response);
				obtenerCiudades(country_code,$('#provincia').val());
			}
		});
}
function obtenerCiudades(country_code,state_code){
	url=base_url+'home/getciudades/?country_code='+country_code+'&state_code='+state_code;
		$.ajax({
			url: url,
			success: function(response){	
				$('#ciudad').html(response);
			}
		});
}

function ValidarForm(){
	var jsonDatos=parsearJSON('{"campos":['+
		'{"nombre":"Nombre","campo":"mail","validacion":"BE"},'+
		'{"nombre":"Nombre","campo":"telefono","validacion":"B"},'+
		'{"nombre":"E-mail","campo":"mensaje","validacion":"B"}'+		
		']}');

		for(var i=0;jsonDatos.campos.length>i;i++){

			var mensaje='';
			
			if(jsonDatos.campos[i]==null){break;}//para IE falla el for al procesar javascript	
			$('#'+jsonDatos.campos[i].campo).removeClass("error");
			if(jsonDatos.campos[i].validacion.indexOf('B')>-1){ 
				if(esVacio ($('#'+jsonDatos.campos[i].campo).val())){
					ok=false;
				}else{
					$('#'+jsonDatos.campos[i].campo).parent().removeClass("error");
				}
			}
			if(jsonDatos.campos[i].validacion.indexOf('E')>-1){ 
				if(!validarEmail ($('#'+jsonDatos.campos[i].campo).val())){
					ok=false;
				}else{
					$('#'+jsonDatos.campos[i].campo).parent().removeClass("error_mail");
				}
			}
		}
}
function enviarMensaje(){
	try{
		var ok=true;
		var jsonDatos=parsearJSON('{"campos":['+
		'{"nombre":"Nombre","campo":"mail","validacion":"BE"},'+
		'{"nombre":"Nombre","campo":"telefono","validacion":"B"},'+
		'{"nombre":"E-mail","campo":"mensaje","validacion":"B"}'+		
		']}');

		for(var i=0;jsonDatos.campos.length>i;i++){

			var mensaje='';
			
			if(jsonDatos.campos[i]==null){break;}//para IE falla el for al procesar javascript	
			$('#'+jsonDatos.campos[i].campo).removeClass("error");
			if(jsonDatos.campos[i].validacion.indexOf('B')>-1){ 
				if(esVacio ($('#'+jsonDatos.campos[i].campo).val())){
					$('#'+jsonDatos.campos[i].campo).parent().addClass("error");
					mensaje+='*';
					ok=false;
				}else{
					$('#'+jsonDatos.campos[i].campo).parent().removeClass("error");
				}
			}
			if(jsonDatos.campos[i].validacion.indexOf('E')>-1){ 
				if(!validarEmail ($('#'+jsonDatos.campos[i].campo).val())){
					$('#'+jsonDatos.campos[i].campo).parent().removeClass("error");
					$('#'+jsonDatos.campos[i].campo).parent().addClass("error_mail");
					if(mensaje=='')
					mensaje+='*';
					else
					mensaje+=' | invalid email';
					ok=false;
				}else{
					$('#'+jsonDatos.campos[i].campo).parent().removeClass("error_mail");
				}
			}
		}

		if(!ok){
			return;
		}else{
			$('.error').removeClass("error");
			$('.error_mail').removeClass("error_mail");
		}

	}catch(e){
		alert(e);
	}	
	$('#bt_form').prop('disabled', true);
	
	var mensaje ='<html>';
	mensaje +='<head>';
	mensaje +='<meta http-equiv=Content-Type content="text/html; charset=UTF-8">';
	mensaje +='</head>';
	mensaje +='<body>';

    mensaje += '<b>Pauny</b><br/><hr/><br/><br/>';
	mensaje += '<b>Pais:</b> '+$('#pais option:selected').text()+'<br/><br/>';
	mensaje += '<b>Provincia:</b> '+$('#provincia option:selected').text()+'<br/><br/>';
	mensaje += '<b>Ciudad:</b> '+$('#ciudad option:selected').text()+'<br/><br/>';
	mensaje += '<b>Email:</b> '+$('#mail').val()+'<br/><br/>';
	mensaje += '<b>Teléfono:</b> '+$('#telefono').val()+'<br/><br/>';
	mensaje += '<br/><hr/><br/>';
	mensaje += $('#mensaje').val()+'<br/><hr><br/>';
	mensaje +='</body>';
	mensaje +='</html>';
	
	
	
	var valores ="'accion':'enviarMail',";
	valores+="'asunto':'> Enviado desde la web de Pauny',";	
	valores+="'mensaje':'"+escape(mensaje)+"',";
	valores+="'email':'newsletter@grupooxford.com',";
	valores+="'name':'Pauny',";
	valores+="'para':'pablo.albini@gmail.com'";

		
	valores='_p='+encode(escape('{'+valores+'}'));
	
	url='/rafaela_alimentos/home/sendform/';
	
	$('#modalAlert').html('<div class="alert alert-default fade in"><span>Enviando Formulario...</span></div>');
	
	$.ajax({
	    url: url,
	    data: valores,
	    success: function(response){		
			if(response){
				var json = parsearJSON(response);
				if(json.resultado=='OK'){
					$('#modalAlert').html('<div class="alert alert-success fade in"><a class="close" data-dismiss="alert">×</a><span>Su mensaje ha sido enviado</span></div>');
					$('input').val('');
					$('textarea').val('');
				}else {
					$('#modalAlert').html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert">×</a><span>Su mensaje no pudo ser enviado<br>Intente mas tarde</span></div>');
				}
			}else{
				$('#modalAlert').html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert">×</a><span>Su mensaje no pudo ser enviado<br>Intente mas tarde</span></div>');
			}
			
			setTimeout(function(){
			  $('#modalAlert').html('');
			}, 8000);
	    	$('#bt_form').prop('disabled', false);
			
		}
	});
}