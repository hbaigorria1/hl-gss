<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   $this->load->helper('language');
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{	
		
		if(!empty($this->session->userdata('token'))):
			ini_set('max_execution_time', 0); 
			ini_set('memory_limit','2048M');
			// ----------------------------
			// testing templating method
			// ----------------------------
		
			//como hemos creado el grupo registro podemos utilizarlo
		    $this->template->set_template('template');

			$this->template->add_css('asset/css/home.css');

			// --		
			// Save utm
			// --
		    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
		    {
		    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
		    }
		   
		    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
		    {
	 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
	 	    }
			
			//añadimos los archivos js que necesitemoa		
			//$this->template->add_js('asset/js/home.js');
			
			//desde aquí también podemos setear el título
			$this->template->write('title', 'Baxter on TOUR', TRUE);
			$this->template->write('description', '', TRUE);
			$this->template->write('keywords', '', TRUE);
			$this->template->write('image', '', TRUE);
			$this->template->write('ogType', 'website', TRUE);
			//obtenemos los usuarios
			//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
			$CI =& get_instance();	
			$leng = $this->config->item('language_abbr');
			//Choose language file according to selected lanaguage
			//print_r($language);
			//exit;
			if($language == "portuguese"):
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = $language;
			elseif ($language == "spanish"):
				$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = $language;
			else:
				
	            if ($leng == 'ar'){
	            	$this->lang->load('web_lang','spanish');
					$data['shortname'] = "es";
					$data['language'] = "spanish";
				}


				if ($leng == 'br'){
					$this->lang->load('web_lang','portuguese');
					$data['shortname'] = "pt";
					$data['language'] = "portuguese";
				}

			endif;
					
			// $data['procedures'] = $this->page_model->get_procedures();
			$data['notifications'] = $this->page_model->get_notificacions();
			$data['procedures'] = $this->page_model->get_procedures_users();
			$data['demos'] = $this->page_model->get_demo();
			
			$this->template->write_view('content', 'layout/home/home', $data);
			$this->template->write_view('header', 'layout/header', $data);
		    
			//$this->template->write_view('footer', 'layout/footer');   
			
			//con el método render podemos renderizar y hacer que se visualice la template
		    $this->template->render();
		else:
			redirect('/');
		endif;
		 //$this->load->view('welcome_message');
	}
	public function demochange()
	{

		$demo = $this->page_model->get_demo_id($_GET['id']);

		$hospital = $this->page_model->get_hospital($demo['demo'][0]['hospital']);
		$product = $this->page_model->get_product($demo['demo'][0]['products'][0]);

		$this->session->set_userdata('id_demo', $_GET['id']);
		$this->session->set_userdata('demo_title' , $demo['demo'][0]['title']);
		$this->session->set_userdata('demo_initial_date' , $demo['demo'][0]['initialDate']);
		$this->session->set_userdata('demo_end_date' , $demo['demo'][0]['endDate']);
		$this->session->set_userdata('hospital' , $hospital['hospital'][0]['name']);
		$this->session->set_userdata('product' , $product['product'][0]['name']);

		redirect('home/');
		
	}
	public function gracias()
	{
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa

		$this->template->add_css('asset/css/home.css');

		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/home.js');

	    
		//la sección header será el archivo views/registro/header_template
	   $this->template->write_view('header', 'layout/header', $data);
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Hillrom - GSS', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();

		
		
		
		
		//el contenido de nuestro formulario estará en views/registro/formulario_registro,
		//de esta forma también podemos pasar el array data a registro/formulario_registro
	    $this->template->write_view('content', 'layout/home/gracias', $data); 
	    
		//la sección footer será el archivo views/registro/footer_template
	    $this->template->write_view('footer', 'layout/footer');   
	    
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}
	
}
