<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   $this->load->helper('language');
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{	
		

	   // GET
		   	if(false):
		   $host = ''.HOST.'survey/all';
		   //$host = ''.HOST.'auth/user/login';

		   //finally print your API response
		   //$data = array(
		   	//'nombre' => 'nombre',
		   	//'232' => 'asdasda',
		   //);
		   //$array = http_build_query($data);

		   $curl = curl_init($host);

		   $authorization = "Authorization: Bearer ".$res['response']['token'];
		   curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
		   //curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$res->token));

		   curl_setopt($curl, CURLOPT_HTTPGET, true);

	       //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($payload));
	       //url_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($payload));
	       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	       $response = curl_exec($curl);
	       curl_close($curl);
	      // return $response;


		   print_r(json_decode($response));
		   exit;
		endif;
 
		
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Hillrom - GSS', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();
		$leng = $this->config->item('language_abbr');
		//Choose language file according to selected lanaguage
		//print_r($language);
		//exit;
		if($language == "portuguese"):
			$this->lang->load('web_lang','portuguese');
			$data['shortname'] = "pt";
			$data['language'] = $language;
		elseif ($language == "spanish"):
			$this->lang->load('web_lang','spanish');
			$data['shortname'] = "es";
			$data['language'] = $language;
		else:
			
            if ($leng == 'ar'){
            	$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = "spanish";
			}


			if ($leng == 'br'){
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = "portuguese";
			}

		endif;
		
		$data = "";
 		
		$this->template->write_view('content', 'layout/login/main', $data);
		
		//$this->template->write_view('header', 'layout/header', $data);
		 
	    
		//$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function password()
	{	
		
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Hillrom - GSS', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	
		
		$data = "";
 		
		$this->template->write_view('content', 'layout/login/password', $data);
		
		//$this->template->write_view('header', 'layout/header', $data);
		 
	    
		//$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function user_login(){

		// LOGIN 

		$url = HOST.'auth/user/login';


		$payload = array(
		    'email' => $_POST['user'],
		    'password' => $_POST['password'],
		);
		
		$curl = curl_init($url);

       curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($payload));
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       //curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
       $response = curl_exec($curl);
       curl_close($curl);
      // return $response;

       $res = json_decode($response, TRUE);



       $demo = $this->page_model->get_demo_user($res['response']['id'],$res['response']['token'] );

       

       $hospital = $this->page_model->get_hospital($demo['demo'][0]['hospital']);
       $product = $this->page_model->get_product($demo['demo'][0]['products'][0]);

      
      
       if($res['success']):

           $newdata = array(
                   'token'  => $res['response']['token'],
                   'id_user' => $res['response']['id'],
                   'id_demo' => $demo['demo'][0]['id'],
                   'demo_title' => ''.$demo['demo'][0]['title'].'',
                   'demo_initial_date' => $demo['demo'][0]['initialDate'],
                   'demo_end_date' => $demo['demo'][0]['endDate'],
                   'hospital' => htmlspecialchars($hospital['hospital'][0]['name']),
                   'product' => htmlspecialchars($product['product'][0]['name'])
           );

            

           $this->session->set_userdata($newdata);
           
           $user = $this->page_model->get_user();
           

           $user_data = array(
           		'user_name' => $user['name'],
           		'user_lastname' => $user['surename'],
           		'user_rol' => $user['role']['spName']
           );

           $this->session->set_userdata($user_data);
           if(empty($demo)):
           	echo "no-demo";
           else:
	       	echo "ok";
           endif;
	    
	    else:
	    	echo "no-user";
	    endif;
		
	}

	public function logout(){

		// LOGIN 

		$url = ''.HOST.'auth/user/logout';
		
		$curl = curl_init($url);

       curl_setopt($curl, CURLOPT_POSTFIELDS);
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       //curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
       $response = curl_exec($curl);
       curl_close($curl);

       $name   = 'token';
       $value  = '';
       $expire = time()+99999;
       $path  = '/';
       $secure = TRUE;

       setcookie($name,$value,$expire,$path);

		redirect('/');
		
	}

	public function user_password(){

		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Hillrom - GSS', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	
		
		$data = "";
 		
		$this->template->write_view('content', 'layout/login/gracias', $data);
		
		//$this->template->write_view('header', 'layout/header', $data);
		 
	    
		//$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function gracias(){

		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Hillrom - GSS', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	
		
		$data = "";
 		
		$this->template->write_view('content', 'layout/login/gracias', $data);
		
		//$this->template->write_view('header', 'layout/header', $data);
		 
	    
		//$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	
}
