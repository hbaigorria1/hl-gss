<section class="encuestas-pendientes">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-2"></div>
            <div class="col-12 col-md-8">
                <h3><?=$this->lang->line('lbl_title_enc')?></h3>
            </div>
            <div class="col-12 col-md-2"></div>
        </div>
        <?php if(!empty($procedures)): $i=0; ?>
            <?php foreach($procedures as $key => $proc):
                    if($proc['procedure'][0]['demo']['id'] == $this->session->userdata('id_demo')):
                        $date_new = explode("T",$proc['procedure'][0]['endDate']);
                        $date_new_final = $date_new[0];
                        if(time() > strtotime($date_new_final)):?>
                        <div class="row">
                            <div class="col-12 col-md-2"></div>
                            <div class="col-12 col-md-8">
                                <h4>
                                    <?php if(!empty($proc['procedure'][0]['category']['image']['url'])): ?>
                                        <img src="<?=$proc['procedure'][0]['category']['image']['url']?>" style="max-width: 30px;">
                                    <?php endif; ?>
                                    <?=$proc['procedure'][0]['title']?></h4>
                                <a href="<?=base_url()?>encuesta/ver/?id=<?=$proc['procedure'][0]['id']?>" class="item-encuesta pendiente"><?=$this->lang->line('lbl_link')?><i class="far fa-clock"></i></a>
                            </div>
                            <div class="col-12 col-md-2"></div>
                        </div>
                        <?php else: ?>
                        <div class="row">
                            <div class="col-12 col-md-2"></div>
                            <div class="col-12 col-md-8">
                                <h4 style="opacity:.6;pointer-events:none;">
                                    <?=$this->lang->line('lbl_prox')?> 
                                    <?php if(!empty($proc['procedure'][0]['category']['image']['url'])): ?>
                                        <img src="<?=$proc['procedure'][0]['category']['image']['url']?>" style="max-width: 30px;">
                                    <?php endif; ?>
                                    <?=$proc['procedure'][0]['title']?> - <?=date('Y-d-m',strtotime($proc['procedure'][0]['endDate']))?></h4>
                            </div>
                            <div class="col-12 col-md-2"></div>
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="row">
                <div class="col-12 col-md-2"></div>
                <div class="col-12 col-md-8">
                </div>
                <div class="col-12 col-md-2"></div>
            </div>
        <?php endif; ?>
    </div>
</section>