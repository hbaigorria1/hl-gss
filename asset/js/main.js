$(document).ready(function(e){

  $('.select-dropdown__button').click(function(e){
    $('.select-dropdown__list').toggleClass('active');
  });
  $('.select-dropdown__list-item').click(function(e){
    $('.select-dropdown__list').addClass('hide');
    var itemValue = $(this).data('value');
    window.location = itemValue;
    $('.select-dropdown__button span').text($(this).text()).parent().attr('data-value', itemValue);
    
  });

  $('.custom-select').click(function(e){
    e.stopPropagation();
    $( '.select-options', this ).slideToggle();
  });
  
  $( '.select-options li' ).click(function(e){
    e.stopPropagation();
    const text_inside = $(this).text();
    $(this).parent().slideUp();
    $(this).parent().next().text( text_inside );
  });
  
  $( document ).click(function(e){
    $( '.select-options' ).slideUp();
  });


  $('.custom-select-foot').click(function(e){
    e.stopPropagation();
    $( '.select-options', this ).slideToggle();
  });
  
  $( '.select-options li' ).click(function(e){
    e.stopPropagation();
    const text_inside = $(this).text();
    $(this).parent().slideUp();
    $(this).parent().next().text( text_inside );
  });
  
  $( document ).click(function(e){
    $( '.select-options' ).slideUp();
  });
});


$('#slide01').owlCarousel({
    loop:true,
    autoplay:true,
    margin:0,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
});
$('#slide02').owlCarousel({
    loop:true,
    autoplay:true,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    margin:0,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
});

$('#slide03').owlCarousel({
    loop:true,
    autoplay:true,
    margin:0,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('#triangleSlideResponsive').owlCarousel({
    loop:true,
    autoplay:false,
    margin:0,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});




$(".pre-registro").submit(function(event){
  event.preventDefault(); //prevent default action 
  var post_url = $(this).attr("action"); //get form action url
  var request_method = $(this).attr("method"); //get form GET/POST method
  var form_data = $(this).serialize(); //Encode form elements for submission
  
  $.ajax({
    url : post_url,
    type: request_method,
    data : form_data,
    beforeSend: function() {
          // setting a timeout
          $('.enviar-btn').addClass('disable');
          $('.gif-load').show('slow');
      },
      success: function(response){
        $('.img-chicos').removeClass('op-65');
        $("#id_user").val(response);
        $('.modal-juego').hide();
      }
  });
});

$(".triangle-btn").click(function (){

    $('html, body').animate({
        scrollTop: $("#contentTriangle").offset().top
    }, 500);
});

$(".triangle-btn-02").click(function (){
    $('html, body').animate({
        scrollTop: $("#contentTriangle").offset().top
    }, 500);
});

$(".form-send-juego").submit(function(event){
  event.preventDefault(); //prevent default action 
  var post_url = $(this).attr("action"); //get form action url
  var request_method = $(this).attr("method"); //get form GET/POST method
  var form_data = $(this).serialize(); //Encode form elements for submission
  
  $.ajax({
    url : post_url,
    type: request_method,
    data : form_data,
    beforeSend: function() {
          // setting a timeout
          $('.enviar-btn').addClass('disable');
          $('.gif-load').show('slow');
      },
      success: function(response){
        $('.form-send-juego').hide();
        $('.ocular-subt').hide();
        $('html, body').animate({
        scrollTop: $("#resultGame").offset().top}, 500);
        if(response == 'living'){
          $('.resultado-juego').html('<div class="content-result"><p>Una casa muy vos tiene un amplio living, iluminado, ideal para descansar ¡y ver tus series preferidas!<br>Por eso, con tu casa Vilahouse, te regalamos el <strong>COMBO LIVING</strong></p></div><a href="#contact">DESCUBRÍ MÁS SOBRE ESA CASA CON TU SELLO PROPIO</a>');
        }
        if(response == 'cocina'){
          $('.resultado-juego').html('<div class="content-result"><p>Una casa muy vos tiene la cocina más práctica para que comer en casa sea la mejor experiencia. ¡Las recetas están esperando tu toque de magia!<br>Por eso, con tu casa Vilahouse, te regalamos el <strong>COMBO COCINA</strong></p></div><a href="#contact">DESCUBRÍ MÁS SOBRE ESA CASA CON TU SELLO PROPIO</a>')
        }
        if(response == 'habitacion'){
          $('.resultado-juego').html('<div class="content-result"><p>Una casa muy vos tiene la habitación ideal para que tu sueño y descanso convivan a la perfección.<br>Por eso, con tu casa Vilahouse, te regalamos el <strong>COMBO HABITACIÓN</strong></p></div><a href="#contact">DESCUBRÍ MÁS SOBRE ESA CASA CON TU SELLO PROPIO</a>')
        }
        if(response == 'jardin'){
          $('.resultado-juego').html('<div class="content-result"><p>Una casa muy vos tiene el espacio exterior que soñás para descansar, jugar y hacer los mejores encuentros con amigos.<br>Por eso, con tu casa Vilahouse, te regalamos el <strong>COMBO JARDÍN</strong></p></div><a href="#contact">DESCUBRÍ MÁS SOBRE ESA CASA CON TU SELLO PROPIO</a>')
        }
      }
  });
});

$( ".cierre-modal-demo" ).click(function() {
  $('.modal-demos').removeClass('d-flex');
  $('.modal-demos').addClass('d-none');
});

$( ".icon-edit" ).click(function() {
  $('.modal-demos').removeClass('d-none');
  $('.modal-demos').addClass('d-flex');
});

var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");

  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            window.location = s.options[i].value;
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}