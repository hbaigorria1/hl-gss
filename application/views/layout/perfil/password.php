<section class="section-home-01">
	<div class="col-12 p-0">
		<img src="<?=base_url()?>asset/img/logo_hl.png" class="img-fluid logo-hl">
	</div>
	<div class="col-12 p-0 text-center">
		<img src="<?=base_url()?>asset/img/logo_tour_hl.png" class="img-fluid logo-tour">
	</div>
</section>

<section class="form-login">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-3"></div>
			<div class="col-12 col-md-6">
				<form class="w-100 login-password" method="post" action="<?=base_url()?>login/user-password/">
					<input type="text" name="user" required placeholder="USER">
					<input type="submit" value="RECUPERAR PASSWORD">
				</form>
			</div>
			<div class="col-12 col-md-3"></div>
		</div>
	</div>
</section>