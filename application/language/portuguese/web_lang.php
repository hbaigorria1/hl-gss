<?php 
$lang['language_key'] = 'The actual message to be shown';

$lang['no_encuestas'] = 'Não há pesquisas pendentes';
$lang['no_estadisticas'] = 'Não há estatísticas para visualizar';

$lang['input_user'] = 'NOME DO USUÁRIO';
$lang['input_pass'] = 'SENHA';
$lang['input_pass_rem'] = 'VOCÊ ESQUECEU SUA SENHA?';
$lang['input_err'] = 'Usuário ou senha incorretos';

$lang['lbl_menu_01'] = 'PROCEDIMENTOS';
$lang['lbl_menu_02'] = 'PESQUISAS';
$lang['lbl_menu_03'] = 'DOCUMENTAÇÃO';
$lang['lbl_menu_04'] = 'ESTATISTICAS';

$lang['lbl_title_enc'] = 'Pesquisas:';
$lang['lbl_prox'] = 'Por vir:';
$lang['lbl_link'] = 'Ir para a pesquisa';

$lang['lbl_perfil'] = 'Meu perfil';
$lang['lbl_salir'] = 'Sair';

$lang['enc_titulo'] = 'VOTAÇÃO';
$lang['enc_btn_sig'] = 'Segue';
$lang['enc_btn_atr'] = 'Atrás';
$lang['enc_btn_env'] = 'Mandar';
$lang['enc_msj01'] = 'Selecione uma resposta';

$lang['enc_pregdeter'] = 'Qual a probabilidade de você recomendar a compra desta mesa cirúrgica?';
$lang['enc_com'] = 'Comentários';

$lang['lbl_meses'] = '["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]';
$lang['lbl_dias'] = '["Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom"]';
$lang['hoy'] = 'Hoje';

$lang['lbl_galeria'] = 'Galeria de imagens';
$lang['lbl_documentacion'] = 'Documentação educativa';
$lang['lbl_descargar'] = 'Baixar';

$lang['hlbax'] = 'Hillrom é parte da Baxter';

$lang['no_demo'] = 'O usuário não contém uma demonstração associada.';

?>