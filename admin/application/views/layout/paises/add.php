<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			  <!--<li role="presentation"><a href="#tab2" data-toggle="tab">Subtabla relacionada</a></li>-->
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
				
			 <form method="post" action="<?php echo base_url()?>paises/save/">
			 	<div class="row">
			 		<div class="col-xs-12 col-md-4">
			 			<div class="td-input">
			 				<b>Nombre:</b><br>
			 				<input type="text" name="nombre" id="nombre">
			 			</div>
			 		</div>
			 		<div class="col-xs-12 col-md-4">
			 			<div class="td-input">
			 				<b>Shortname:</b><br>
			 				<input type="text" name="shortname" id="shortname">
			 			</div>
			 		</div>
			 		<div class="col-xs-12 col-md-4">
			 			<div class="td-input">
			 				<b>Lenguaje:</b><br>
			 				<select name="lenguaje">
			 					<option value="">Seleccionar lenguaje</option>
			 					<option value="spanish" >Español</option>
			 					<option value="english" >Ingles</option>
			 					<option value="portuguese" >Portugues</option>
			 				</select>
			 			</div>
			 		</div>
			 	</div>
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	   <a href="<?php echo base_url()?>paises/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
	</div>
</div>
<br style="clear:both;"/>