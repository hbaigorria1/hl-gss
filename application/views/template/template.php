<!DOCTYPE html>
<html>
   <head>
   	  <meta charset="utf-8" />
      <title><?= $title ?></title>
	  <meta name="description" content="">
	  <meta name="keywords" content="">
	  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
	  
	  <meta property="og:title" content="Soluciones Quirúrgicas: Aquí, cada segundo cuenta" />
	  <meta property="og:type" content="<?= $ogType ?>" />
	  <meta property="og:url" content="<?php echo base_url(uri_string()) ?>" />
	  <meta property="og:image" content="" />
	  <meta property="og:description" content="Es vital contar con soluciones quirúrgicas que te ayuden a maximizar tu tiempo y cumplir con tu misión de salvar y sostener vidas." />
	  
	  <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>asset/img/favicon.ico?">

	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css'>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
	  <link rel='stylesheet' href='https://unpkg.com/swiper@7/swiper-bundle.min.css'>
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css'>
	  <link rel="preconnect" href="https://fonts.gstatic.com">
	  <link href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@300;400;500;600;700;900&display=swap" rel="stylesheet">

	  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css'>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/custom.css?version=<?=time()?>" type="text/css" media="all" />
	  <script>
  			var base_url = "<?php echo base_url() ?>";
  			var meses = '<?=$this->lang->line('lbl_meses')?>';
  			var dias = '<?=$this->lang->line('lbl_dias')?>';
  			var hoy = '<?=$this->lang->line('hoy')?>';
	  </script>
	  
	  <?= $_styles ?>

   </head>
   <body>
	  <?= $header ?>        
	  <?= $content ?>
    <?= $footer ?>
	  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/7.2.0/swiper-bundle.min.js'></script>

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/pagePiling.js/1.5.6/jquery.pagepiling.min.js'></script>

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js'></script>
	  <script src='https://use.fontawesome.com/826a7e3dce.js'></script>

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js'></script>
      
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	
	  <script defer type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js?version=<?=time()?>"></script>

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js'></script>

	  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js@3.6.0/dist/chart.min.js"></script>
	  
	  <?= $_scripts ?>


	  <script type="text/javascript">
	  	$(".login-form").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.show-login-message').hide();
	  		        $('.show-login-message-no-demo').hide();
	  		    },
	  		    success: function(response){

	  		    	if(response == 'ok'){
  		          	window.location.replace("<?php echo base_url().$this->config->item('language_abbr') ?>/home/");
	  		    	}
	  		    	if(response == 'no-user'){
  		          	$('.show-login-message').show();
	  		    	}

	  		    	if(response == 'no-demo'){
  		          	$('.show-login-message-no-demo').show();
	  		    	}
	  		    }
	  		});
	  	});

	  	$(".add-proc").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.show-login-message').hide();
	  		        $('.agregar-proc').addClass('disable');
	  		    },
	  		    success: function(response){
  		          	window.location.replace("<?php echo base_url().$this->config->item('language_abbr') ?>/procedimientos/");
	  		    }
	  		});
	  	});

	  	$(".chang-demo").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.show-login-message').hide();
	  		        $('.agregar-proc').addClass('disable');
	  		    },
	  		    success: function(response){
  		          	window.location.replace("<?php echo base_url().$this->config->item('language_abbr') ?>/home/");
	  		    }
	  		});
	  	});

	  	$(".login-password").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar').addClass('disable');
	  		        $('.gif').show('slow');
	  		        $('.message').hide("slow");
	  		    },
	  		    success: function(response){
  		          	window.location.replace("<?php echo base_url().$this->config->item('language_abbr') ?>/login/gracias/");
	  		    }
	  		});
	  	});

	  	$("#msform").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar').addClass('disable');
	  		        $('.gif').show('slow');
	  		        $('.message').hide("slow");
	  		    },
	  		    success: function(response){
  		          	//window.location.replace("<?php echo base_url() ?>home/");
	  		    }
	  		});
	  	});


	  </script>

	  <script type="text/javascript">
	  	$('#HeaderHome').owlCarousel({
	  	  center: true,
  	      items:2,
  	      loop:true,
  	      margin:10,
  	      responsive:{
  	          600:{
  	              items:4
  	          }
  	      }
	  	})
	  </script>

	  <script type="text/javascript">
	  	$('#ImgSlide').owlCarousel({
	  	  autoplay: false,
	  	  autoplayHoverPause: true,
	  	  loop: true,
	  	  margin:10,
	  	  responsiveClass: true,
	  	  nav: true,
	  	  loop: false,
	  	  stagePadding: 5,
	  	  responsive: {
	  	    0: {
	  	      items: 1
	  	    },
	  	    568: {
	  	      items: 1
	  	    },
	  	    600: {
	  	      items: 1
	  	    },
	  	    1000: {
	  	      items: 1
	  	    }
	  	  }
	  	});
	  	$('#ImgVideo').owlCarousel({
	  	  autoplay: false,
	  	  autoplayHoverPause: true,
	  	  loop: true,
	  	  margin:10,
	  	  responsiveClass: true,
	  	  nav: true,
	  	  loop: false,
	  	  stagePadding: 5,
	  	  responsive: {
	  	    0: {
	  	      items: 1
	  	    },
	  	    568: {
	  	      items: 1
	  	    },
	  	    600: {
	  	      items: 1
	  	    },
	  	    1000: {
	  	      items: 1
	  	    }
	  	  }
	  	});
	  	$(document).ready(function() {
	  	  $('.popup-youtube').magnificPopup({
	  	    disableOn: 320,
	  	    type: 'iframe',
	  	    mainClass: 'mfp-fade',
	  	    removalDelay: 160,
	  	    preloader: false,
	  	    fixedContentPos: true
	  	  });
	  	});
	  	$('.item').magnificPopup({
	  	  delegate: 'a',
	  	});
	  </script>
	  <script type="text/javascript">
	  	$( ".menu-open" ).click(function() {
	  	  $( ".nav-menu-animated" ).addClass('active-menu');
	  	});
	  	$( ".close-menu" ).click(function() {
	  	  $( ".nav-menu-animated" ).removeClass('active-menu');
	  	});

	  	$( ".notif-open" ).click(function() {
	  	  $( ".nav-menu-animated-notif" ).addClass('active-menu');
	  	});
	  	$( ".close-menu-notif" ).click(function() {
	  	  $( ".nav-menu-animated-notif" ).removeClass('active-menu');
	  	});
	  </script>
	  <script type="text/javascript">
	  	  const togglePassword = document.querySelector('#togglePassword');
	  	  const password = document.querySelector('#id_password');
	  	 
	  	  togglePassword.addEventListener('click', function (e) {
	  	    // toggle the type attribute
	  	    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
	  	    password.setAttribute('type', type);
	  	    // toggle the eye slash icon
	  	    this.classList.toggle('fa-eye-slash');
	  	});
	  </script>
	  <script type="text/javascript">
	  	
	  	$(".startDateNew").on("change paste keyup", function() {

	  		var starDateNew = $('.startDateNew').val();
	  		$('.endDateNew').val(starDateNew);
	  	});

	  </script>
   </body>
</html>